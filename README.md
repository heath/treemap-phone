Instructions for arriving at this state of error:

    npm install -g gulp cult
    git clone https://github.com/heath/treemap-web.git
    cd treemap-web
    npm install
    cult build
    cd ..
    cordova create treemap-phone io.cordova.treemap TreeMap
    cd treemap-phone
    cd www
    rm -rf *
    cp -r ../../treemap-web/temp/* .
    cd ..
    cordova platform add android
    cordova build android
    cordova serve
    #visit localhost:8000/android/www

...or you can just do `cd www && cordova serve` to see the errors since
everything is included.

The issue I'm running into is that it can't find the styles, and
for some reason, app.js is an html file.
