(function() {
  angular.module('app', ['ui.router']);

}).call(this);

(function() {
  var app;

  app = angular.module("app");

  app.controller("MapCtrl", [
    "$scope", "$state", function($scope, $state) {
      var getPins, map, markerLayerGroup, setPins;
      map = L.map("map").setView([35.95, -86.805540], 8);
      markerLayerGroup = L.layerGroup().addTo(map);
      L.tileLayer("http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
        maxZoom: 18
      }).addTo(map);
      getPins = function(e) {
        var bounds, url;
        bounds = map.getBounds();
        url = "http://localhost:3000/trees/within?lat1=" + (bounds.getNorthEast().lat) + "&lon1=" + (bounds.getNorthEast().lng) + "&lat2=" + (bounds.getSouthWest().lat) + "&lon2=" + (bounds.getSouthWest().lng);
        $.get(url, setPins, "json");
      };
      setPins = function(data) {
        var markerArray;
        map.removeLayer(markerLayerGroup);
        markerArray = [];
        parks.forEach(function(park, i) {
          return markerArray[i] = L.marker([park.pos[1], park.pos[0]]).bindPopup(park.Name);
        });
        markerLayerGroup = L.layerGroup(markerArray).addTo(map);
      };
      map.on("dragend", getPins);
      map.on("zoomend", getPins);
      map.whenReady(getPins);
    }
  ]);

}).call(this);

(function() {
  var app;

  app = angular.module("app");

  app.config([
    "$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {
      $stateProvider.state("map", {
        url: "/",
        controller: "MapCtrl",
        templateUrl: "/components/map/index.html"
      });
      return $urlRouterProvider.otherwise("/");
    }
  ]).run([
    '$rootScope', '$state', '$stateParams', function($rootScope, $state, $stateParams) {
      $rootScope.$state = $state;
      return $rootScope.$stateParams = $stateParams;
    }
  ]);

}).call(this);

(function() {
  if (document.readyState !== 'complete') {
    angular.element(document).ready(function() {
      return angular.bootstrap(document, ['app']);
    });
  } else {
    angular.bootstrap(document, ['app']);
  }

}).call(this);
